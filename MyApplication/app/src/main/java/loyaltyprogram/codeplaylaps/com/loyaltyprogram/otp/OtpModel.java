package loyaltyprogram.codeplaylaps.com.loyaltyprogram.otp;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 03-Aug-17.
 */

public class OtpModel extends BaseModel {
    private String token="";
    private String message="";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
