package loyaltyprogram.codeplaylaps.com.loyaltyprogram.base;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.welcome.RedemptionModel;


/**
 * Created by mohit on 12/23/2016.
 */

public class BaseParser {

    private Gson gson;
    public BaseModel parseJson(JSONObject response) throws JSONException {

        return null;
    }

    protected JSONObject validate(JSONObject response)  {
        try {

           // response.getJSONObject("response").

            if (!response.isNull("error") || response.isNull("response")) {

                if(response.getBoolean("status"))
                {
                    return new JSONObject();

                }

                return null;

            }else{
                return response.getJSONObject("response");
            }

        }catch (JSONException e) {
            e.printStackTrace();
            Log.i("Exception","json");
        }catch (Exception e){
            e.printStackTrace();
            Log.i("Exception","main");
           // return null;
        }
        return  null;

    }
    public Gson getGsonInstance() {
        if (gson == null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gson = gsonBuilder.create();
        }
        return gson;
    }


}
