package loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward;

import android.content.Context;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseHandler;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Urls;

/**
 * Created by HP on 01-Aug-17.
 */

public class RewardHandler extends BaseHandler {
    public void rewardService(Context context, String customerNum, String pin, RewardResponse response){
        baseResponse=response;
        //%2B919871246626&pin=547639";
        jsonGetRequest(context, Urls.FETCH_REWARDS+customerNum+"&pin="+pin,new RewardParser());
    }

    public void redeemService(Context context,String header,RewardResponse rewardResponse){
        baseResponse=rewardResponse;
        stringHeaderRequest(context,Urls.REDEEM_REWARDS,"Basic "+header,new RedeemCodeParser());
    }
}
