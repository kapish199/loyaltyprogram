package loyaltyprogram.codeplaylaps.com.loyaltyprogram.operatorAuthentication;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseParser;

/**
 * Created by HP on 24-Jul-17.
 */

public class AuthenticateParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseResponse(response);
    }

    private BaseModel parseResponse(JSONObject response) {

        AuthenticationModel model;
        Gson gson=getGsonInstance();
        model=gson.fromJson(response.toString(),AuthenticationModel.class);
        return model;
    }
}
