package loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils;

/**
 * Created by HP on 21-Jul-17.
 */

public class Urls {
    public static String BASE_URL ="https://staging.urbanpiper.com/api/";
    public static String USER_PROFILE = BASE_URL +"v1/user/profile?customer_phone=";
    //public static String updateUserProfile= BASE_URL +"/?customer_phone="/*"https://staging.urbanpiper.com/api/v1/user/profile/?customer_phone=%2B919123456789"*/;
    public static String UPDATE_USER=BASE_URL+"v2/card/?format=json&";
    //public static String EARNING_POINTS =BASE_URL+"v2/purchase/";//https://staging.urbanpiper.com/api/v2/purchase/?customer_phone=%2B917042330568&amount=512
    public static String EARNING_POINTS =BASE_URL+"v2/purchase/?customer_phone=";
    public static String GENERATE_OTP=BASE_URL+"v2/auth/phone/";
    public static String VERIFY_OTP=BASE_URL+"v2/auth/phone/";//919880012203/?otp=209677&ov=";  //BASE_URL+"v2/auth/phone/";

    public static String FETCH_REWARDS=BASE_URL+"v2/rewards/?phone="; //%2B919871246626&pin=547639";
    //https://staging.urbanpiper.com/api/v2/rewards/44186085/redeem/
    public static String REDEEM_REWARDS=BASE_URL+"v2/rewards/44186085/redeem/"; //<reward_id>/redeem/"
    //https://staging.urbanpiper.com/api/v1/redeem/1868et/?format=json&markused=True
    public static String VERIFY_REDEMPTION_CODE=BASE_URL+"v1/redeem/";//1868et/?format=json&markused=True
}
