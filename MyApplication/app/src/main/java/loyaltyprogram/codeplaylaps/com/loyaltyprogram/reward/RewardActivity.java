package loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URLDecoder;
import java.util.ArrayList;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.R;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.RedeemActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.operatorAuthentication.AuthenticationActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.operatorAuthentication.AuthenticationModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model.RedeemModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model.RewardModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model.RewardWrapper;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Singleton;

public class RewardActivity extends AppCompatActivity implements View.OnClickListener {

    AuthenticationModel authenticationModel;
    TextView headerText,tokenView;
    LinearLayout  rewardContainerLay,lockedRewardContainer;
    RelativeLayout loaderLayout;
    private int leftMargin;
    private int topMargin,marginTop_lockedReward;
    private int padVal;
    private String pin,token;
    private RewardWrapper rewardWrapper;
    String numDecoded,encodedString;
    RedeemModel redeemModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        loaderLayout= (RelativeLayout) findViewById(R.id.loaderLayout);
        leftMargin = dpToPx(45, RewardActivity.this);
        marginTop_lockedReward=dpToPx(30, RewardActivity.this);
        topMargin = dpToPx(20, RewardActivity.this);
        padVal = dpToPx(8, RewardActivity.this);

        pin=getIntent().getStringExtra("otp");
        token=getIntent().getStringExtra("token");


        numDecoded=URLDecoder.decode(Singleton.getInstance().getCustomerNum());
        //+917042330568__bizid:token
       // String stringToEncode=numDecoded+"__69650765:"+token;
        numDecoded = numDecoded+"__69650765";
        String stringToEncode=String.format("%s:%s",numDecoded,token);

        Log.e("encodedString","beforeReplace"+stringToEncode);
        byte[] byteString=stringToEncode.replace(" ","+").getBytes();
        Log.e("encodedString","afeterReplace"+stringToEncode);
        //byte[] byteString=stringToEncode.getBytes();
        encodedString=Base64.encodeToString(byteString,Base64.NO_WRAP);


        SharedPreferences preferences=getSharedPreferences("myPref",MODE_PRIVATE);
        String earnedPoints=preferences.getString("earnedPoints","");
        String pointAdded=preferences.getString("pointsAdded","");

        //authenticationModel = (AuthenticationModel) getIntent().getSerializableExtra("authenticationModel");
        rewardContainerLay = (LinearLayout) findViewById(R.id.rewardContainerLay);
        lockedRewardContainer= (LinearLayout) findViewById(R.id.lockedRewardContainer);
        //rewardlinLay= (LinearLayout) findViewById(R.id.reward_linLay);
        headerText = (TextView) findViewById(R.id.headerText);
        headerText.setText("Dear " + Singleton.getInstance().getCustomerName() + " , you have earned " +pointAdded /*+authenticationModel.getPoints_added()*/ + " chilis for your purchase Total number of chilis in your account: " +earnedPoints/*+ authenticationModel.getPoints()*/);

        //callRewardService();
        rewardWrapper=Singleton.getInstance().getRewardWrapper();
        makeView(rewardWrapper);
    }

    private void callRewardService() {
       new RewardHandler().rewardService(RewardActivity.this,Singleton.getInstance().getCustomerNum(),pin,new RewardResponse() {
           @Override
           public void onResponse(BaseModel baseModel, String jsonString) {
               loaderLayout.setVisibility(View.GONE);
                  if(baseModel!=null){
                      rewardWrapper= (RewardWrapper) baseModel;
                      makeView(rewardWrapper);
                  }
           }
       });
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void makeView(RewardWrapper rewardWrapper) {
        ArrayList<RewardModel> unlockedRewards=rewardWrapper.getUnlocked();
        ArrayList<RewardModel> lockedRewards=rewardWrapper.getLocked();
        Button[] redeemBtn = new Button[unlockedRewards.size()];
        //horizontal rewardsLyout Layoutparams
        LinearLayout.LayoutParams layParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layParams.setMargins(0, topMargin, 0, 0);
        //rewardTextview params
        LinearLayout.LayoutParams rewardParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        rewardParams.weight=30;
        rewardParams.gravity=Gravity.CENTER_VERTICAL;
        //redeem btn params
        LinearLayout.LayoutParams redeemBtnParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        redeemBtnParams.weight=70;
        redeemBtnParams.gravity=Gravity.CENTER_VERTICAL;

        //typeFace
        Typeface typeface=Typeface.create("sans-serif",Typeface.NORMAL);

        for (int i = 0; i < unlockedRewards.size(); i++) {
            LinearLayout rewardsLyout = new LinearLayout(RewardActivity.this);
            rewardsLyout.setOrientation(LinearLayout.HORIZONTAL);
            rewardsLyout.setLayoutParams(layParams);


            TextView reward = new TextView(RewardActivity.this);
            reward.setText(unlockedRewards.get(i).getTitle());
            reward.setTypeface(typeface);
            //reward.setGravity(Gravity.CENTER);
            reward.setPadding(0,0,padVal,0);
            //reward.setTextColor(getResources().getColor(R.color.nanDosRed));
            reward.setTextSize(24);


            reward.setLayoutParams(rewardParams);
            rewardsLyout.addView(reward);

            /*Button redeem=new Button(RewardActivity.this);*/
            LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            btnParams.setMargins(leftMargin, 0, 0, 0);

            redeemBtn[i] = new Button(RewardActivity.this);
            redeemBtn[i].setLayoutParams(btnParams);
            redeemBtn[i].setBackground(getResources().getDrawable(R.drawable.bg_btn_stroke));
            redeemBtn[i].setTypeface(typeface);
            redeemBtn[i].setGravity(Gravity.CENTER);
            redeemBtn[i].setPadding(padVal, padVal, padVal, padVal);
            redeemBtn[i].setText("Redeem");
            redeemBtn[i].setTextColor(getResources().getColor(R.color.nanDosRed));
            redeemBtn[i].setTextSize(22);

            redeemBtn[i].setTag(i);

            redeemBtn[i].setOnClickListener(RewardActivity.this);
            rewardsLyout.addView(redeemBtn[i]);

            redeemBtn[i].setLayoutParams(redeemBtnParams);
            rewardContainerLay.addView(rewardsLyout);
        }
        //lockedRewardView params
        LinearLayout.LayoutParams lockedRewardParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        lockedRewardParams.setMargins(0,marginTop_lockedReward,0,0);
        lockedRewardParams.gravity=Gravity.CENTER_HORIZONTAL;

        for(RewardModel eachmodel:lockedRewards){
            TextView lockedRewardView =new TextView(RewardActivity.this);
            lockedRewardView.setText("* "+eachmodel.getTitle());
            lockedRewardView.setGravity(Gravity.CENTER);
            //lockedRewardView.setTextColor(getResources().getColor(R.color.nanDosRed));
            lockedRewardView.setTextSize(24);
            lockedRewardView.setTypeface(typeface);
            lockedRewardView.setLayoutParams(lockedRewardParams);
            lockedRewardContainer.addView(lockedRewardView);
        }
      loaderLayout.setVisibility(View.GONE);

//        rewardContainerLay.removeAllViews();
//        rewardContainerLay.addView(rewardlinLay);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(RewardActivity.this, AuthenticationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        int tag = (int) v.getTag();
        showAlert("Do you want to Redem this reward?",tag);
        /*switch (tag) {
            case 1: {
                //showToast("" + tag);
                showAlert("Do you want to Redem this reward?");
                break;
            }
            case 2: {
                //showToast("" + tag);
                showAlert("Do you want to Redem this reward?");
                break;
            }
            case 3: {
                //showToast("" + tag);
                showAlert("Do you want to Redem this reward?");
                break;
            }
            case 4: {
                //showToast("" + tag);
                showAlert("Do you want to Redem this reward?");
                break;
            }
            case 5: {
                //showToast("" + tag);
                showAlert("Do you want to Redem this reward?");
                break;
            }
        }*/
    }

    void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    void showAlert(String title,/*, String msg*/final int tag) {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle(title);
        //dialog.setMessage(msg);
        dialog.setCancelable(false);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //showToast(rewardWrapper.getUnlocked().get(tag).getId());

                callRedeemService();
                /*Intent intent = new Intent(RewardActivity.this, OtpActivity.class);
                startActivity(intent);*/
            }
        });

        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void callRedeemService(){
        new RewardHandler().redeemService(RewardActivity.this, encodedString, new RewardResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString) {
                if (baseModel != null) {
                    redeemModel= (RedeemModel) baseModel;
                    Intent intent = new Intent(RewardActivity.this, RedeemActivity.class);
                    intent.putExtra("redemptionCode",redeemModel.getRedemption_code());
                    startActivity(intent);
                }
            }
        });
    }
}
