package loyaltyprogram.codeplaylaps.com.loyaltyprogram.operatorAuthentication;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseInterface;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 24-Jul-17.
 */

public interface ServiceResponseInterface extends BaseInterface {
    @Override
    void onResponse(BaseModel baseModel, String jsonString);
}
