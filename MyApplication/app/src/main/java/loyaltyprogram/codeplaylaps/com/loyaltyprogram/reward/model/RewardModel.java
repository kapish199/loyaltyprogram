package loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 02-Aug-17.
 */

public class RewardModel extends BaseModel {
    private String claimed_count="";
    private String description="";
    private String expires_on="";
    private String id="";
    private String img_link="";
    private String in_store_coupon_rewards="";
    private String locked="";
    private String points="";
    private String redeemed_count="";
    private String title="";
    private String type="";
    private String value="";


    public String getClaimed_count() {
        return claimed_count;
    }

    public void setClaimed_count(String claimed_count) {
        this.claimed_count = claimed_count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpires_on() {
        return expires_on;
    }

    public void setExpires_on(String expires_on) {
        this.expires_on = expires_on;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg_link() {
        return img_link;
    }

    public void setImg_link(String img_link) {
        this.img_link = img_link;
    }

    public String getIn_store_coupon_rewards() {
        return in_store_coupon_rewards;
    }

    public void setIn_store_coupon_rewards(String in_store_coupon_rewards) {
        this.in_store_coupon_rewards = in_store_coupon_rewards;
    }

    public String getLocked() {
        return locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRedeemed_count() {
        return redeemed_count;
    }

    public void setRedeemed_count(String redeemed_count) {
        this.redeemed_count = redeemed_count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
