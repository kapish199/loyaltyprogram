package loyaltyprogram.codeplaylaps.com.loyaltyprogram.otp;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseParser;

/**
 * Created by HP on 02-Aug-17.
 */

public class GeneratedOtpParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        //parse result
        return parseOtpResponse(response);
    }

    private BaseModel parseOtpResponse(JSONObject response) {
        String msg = null;
        String token=null;
        try {
            msg = response.getString("message");
            token=response.getString("token");

        } catch (JSONException e) {
               e.printStackTrace();
        }
        if (msg!=null && msg.equals("Authenticated successfully.")) {
            OtpModel otpModel=new OtpModel();
            otpModel.setMessage(msg);
            //otpModel.setToken(Base64.encodeToString(token.getBytes(),Base64.DEFAULT));
            otpModel.setToken(token);
            return otpModel;
        }else if(msg!=null && msg.equals("Invalid pin")){
            OtpModel model=new OtpModel();
            model.setMessage("Invalid pin");
            return model;
        }
        else {
            return null;
        }
    }

}
