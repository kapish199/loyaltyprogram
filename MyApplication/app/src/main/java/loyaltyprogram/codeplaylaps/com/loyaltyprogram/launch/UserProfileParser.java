package loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseParser;

/**
 * Created by HP on 21-Jul-17.
 */

public class UserProfileParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseUserProfile(response);
    }

    private BaseModel parseUserProfile(JSONObject response) {
        Gson gson=getGsonInstance();
        UserProfileModel userProfileModel;
        userProfileModel=gson.fromJson(response.toString(),UserProfileModel.class);
        return userProfileModel;
    }
}
