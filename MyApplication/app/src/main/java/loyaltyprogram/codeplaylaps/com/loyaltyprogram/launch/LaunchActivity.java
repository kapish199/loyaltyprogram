package loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.net.URLEncoder;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.R;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.signup.SignupActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Singleton;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.welcome.WelcomeActivity;

public class LaunchActivity extends AppCompatActivity {

    EditText phoneNo;
    Button submitBtn;
    UserProfileModel userProfileModel;
    RelativeLayout loaderLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        phoneNo= (EditText) findViewById(R.id.phn_no);
        submitBtn= (Button) findViewById(R.id.submitBtn);
        loaderLayout= (RelativeLayout) findViewById(R.id.loaderLayout);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num=phoneNo.getText().toString();
                if(phoneNo.getText().toString().length()>=10 && phoneNo.getText().toString().length()<=15){
                    if(!num.substring(0).matches("[+]")){
                        num="+91"+num;
                    }
                    num=URLEncoder.encode(num);
                    loaderLayout.setVisibility(View.VISIBLE);
                    callService(num);
                    //Toast.makeText(LaunchActivity.this, num, Toast.LENGTH_SHORT).show();
                }else{
                    phoneNo.setError("Please enter valid number");
                }
            }
        });

    }

    private void callService(final String num){
        new UserProfileHandler().getUserProfile(LaunchActivity.this, num, new UserProfileResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString) {
                loaderLayout.setVisibility(View.GONE);
                if(baseModel!=null){
                    //means status code 200
                    //Toast.makeText(LaunchActivity.this,"Base model not null", Toast.LENGTH_SHORT).show();
                    userProfileModel= (UserProfileModel) baseModel;
                    Singleton.getInstance().setCustomerName(userProfileModel.getFirst_name());
                    Singleton.getInstance().setCustomerNum(userProfileModel.getPhone());
                    Intent intent=new Intent(LaunchActivity.this,WelcomeActivity.class);
                    intent.putExtra("customerName",userProfileModel.getFirst_name());
                    intent.putExtra("phoneNumber",userProfileModel.getPhone());
                    startActivity(intent);
                }else if(Integer.parseInt(jsonString)==400){
                    /*Toast.makeText(LaunchActivity.this,"400", Toast.LENGTH_SHORT).show();*/
                    Intent intent=new Intent(LaunchActivity.this, SignupActivity.class);
                    intent.putExtra("phoneNumber",num);
                    startActivity(intent);
                }
            }
        });
    }
}
