package loyaltyprogram.codeplaylaps.com.loyaltyprogram.signup;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.R;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch.UserProfileHandler;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch.UserProfileResponse;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Singleton;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.welcome.WelcomeActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

public class SignupActivity extends AppCompatActivity {

    EditText name,email;
    Button signUp;
    String num;
    SignUpModel signUpModel;
    RelativeLayout loaderLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        num=getIntent().getStringExtra("phoneNumber");
        name= (EditText) findViewById(R.id.name);
        email= (EditText) findViewById(R.id.email);
        loaderLayout= (RelativeLayout) findViewById(R.id.loaderLayout);
        signUp= (Button) findViewById(R.id.signUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailAdd=email.getText().toString();
                if(name.getText().toString().length()>=4 )
                {
                    if(emailAdd.contains("@") && emailAdd.contains(".")){
                        loaderLayout.setVisibility(View.VISIBLE);
                        new UserProfileHandler().signUpService(SignupActivity.this, name.getText().toString(), email.getText().toString(),num, new UserProfileResponse() {
                            @Override
                            public void onResponse(BaseModel baseModel, String jsonString) {
                                loaderLayout.setVisibility(View.GONE);
                                if (baseModel!=null){
                                    // Toast.makeText(SignupActivity.this,"basemodel not null", Toast.LENGTH_SHORT).show();
                                    signUpModel= (SignUpModel) baseModel;
                                    Singleton.getInstance().setCustomerName(signUpModel.getCustomer_name());
                                    Singleton.getInstance().setCustomerNum(num);
                                    Intent intent=new Intent(SignupActivity.this, WelcomeActivity.class);
                                    intent.putExtra("customerName",signUpModel.getCustomer_name());
                                    intent.putExtra("phoneNumber",num/*signUpModel.getCustomer_phone()*/);
                                    startActivity(intent);
                                }else if(Integer.parseInt(jsonString)==400){
                         /*   Intent intent=new Intent(SignupActivity.this, WelcomeActivity.class);
                            startActivity(intent);*/
                                    Toast.makeText(SignupActivity.this,"Some error occured,please try again.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }else{
                        email.setError("Invalid email");
                    }
                }else{
                    name.setError("Please enter valid name");
                }



            }
        });
    }
}
