package loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 03-Aug-17.
 */

public class RedeemModel extends BaseModel {
    private String redemption_code="";

    public String getRedemption_code() {
        return redemption_code;
    }

    public void setRedemption_code(String redemption_code) {
        this.redemption_code = redemption_code;
    }
}
