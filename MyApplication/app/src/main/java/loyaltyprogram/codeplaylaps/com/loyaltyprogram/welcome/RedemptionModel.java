package loyaltyprogram.codeplaylaps.com.loyaltyprogram.welcome;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 04-Aug-17.
 */

public class RedemptionModel extends BaseModel {
    private String message="";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
