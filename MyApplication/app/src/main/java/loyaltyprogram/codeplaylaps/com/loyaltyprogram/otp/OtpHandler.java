package loyaltyprogram.codeplaylaps.com.loyaltyprogram.otp;

import android.content.Context;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseHandler;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Urls;

/**
 * Created by HP on 02-Aug-17.
 */

public class OtpHandler extends BaseHandler {
    public void getOtp(Context context,String num,OtpResponse otpResponse){
        baseResponse=otpResponse;
        jsonGetRequest(context, Urls.GENERATE_OTP+num,new OtpParser());
    }

    public void verifyOtp(Context context,String num,String otp,OtpResponse otpResponse)
    {
        baseResponse=otpResponse;
        JsonStringRequest(context,Urls.VERIFY_OTP+num+"/?otp="+otp+"&ov=",new GeneratedOtpParser());
    }
}
