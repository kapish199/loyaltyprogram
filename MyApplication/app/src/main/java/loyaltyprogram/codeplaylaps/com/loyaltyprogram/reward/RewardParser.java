package loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseParser;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model.RewardWrapper;

/**
 * Created by HP on 02-Aug-17.
 */

public class RewardParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseRewardJson(response);
    }

    private BaseModel parseRewardJson(JSONObject response) {
        RewardWrapper rewardWrapper;
        Gson gson=getGsonInstance();
        rewardWrapper=gson.fromJson(response.toString(),RewardWrapper.class);
        return rewardWrapper;
    }

}
