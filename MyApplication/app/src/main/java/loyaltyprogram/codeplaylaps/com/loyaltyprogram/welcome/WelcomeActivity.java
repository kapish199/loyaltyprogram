package loyaltyprogram.codeplaylaps.com.loyaltyprogram.welcome;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.operatorAuthentication.AuthenticationActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.R;

public class WelcomeActivity extends AppCompatActivity {

    String cust_name,num;
    TextView welcome;
    EditText purchase_amt;
    Button submitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        welcome= (TextView) findViewById(R.id.welcome);
        purchase_amt= (EditText) findViewById(R.id.purchase_amt);
        submitBtn= (Button) findViewById(R.id.submitBtn);

        cust_name=getIntent().getStringExtra("customerName");
        num=getIntent().getStringExtra("phoneNumber");
        welcome.setText("Welcome "+cust_name);
        onSubmit();
    }

     public void onSubmit(){
         submitBtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(purchase_amt.getText().toString().length()>0) {
                     double amt = Double.parseDouble(purchase_amt.getText().toString());
                     showAlertDialog(""+amt);
                 }else{
                     purchase_amt.setError("Please enter valid amount");
                 }
             }
         });
     }

     public void showAlertDialog(final String amount){
         AlertDialog dialog=new AlertDialog.Builder(WelcomeActivity.this).create();
         dialog.setTitle("Confirmation");
         dialog.setMessage("Validate Transaction for Rs."+amount);
         dialog.setCancelable(false);
         dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Continue", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 Intent intent=new Intent(WelcomeActivity.this, AuthenticationActivity.class);
                 intent.putExtra("amount",amount);
                 intent.putExtra("phoneNumber",num);
                 startActivity(intent);
             }
         });
         dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Back", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 dialog.dismiss();
             }
         });
         dialog.show();
     }

}
