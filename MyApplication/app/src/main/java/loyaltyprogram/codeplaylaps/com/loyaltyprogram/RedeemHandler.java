package loyaltyprogram.codeplaylaps.com.loyaltyprogram;

import android.content.Context;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseHandler;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Urls;

/**
 * Created by HP on 04-Aug-17.
 */

public class RedeemHandler extends BaseHandler {
    public void verifyCodeService(Context context,String code,RedeemInterface redeemInterface){
        baseResponse=redeemInterface;//1868et/?format=json&markused=True
        jsonGetRequest(context, Urls.VERIFY_REDEMPTION_CODE+code+"/?format=json&markused=True",new RedeemParser());
    }
}
