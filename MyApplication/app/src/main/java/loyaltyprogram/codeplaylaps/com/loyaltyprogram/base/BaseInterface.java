package loyaltyprogram.codeplaylaps.com.loyaltyprogram.base;

/**
 * Created by mohit on 12/16/2016.
 */

public interface BaseInterface {
    /**
     *  base interface to handle response fromm servers.
     * @param baseModel
     * @param jsonString
     */
    void onResponse(BaseModel baseModel, String jsonString);
}
