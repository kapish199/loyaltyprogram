package loyaltyprogram.codeplaylaps.com.loyaltyprogram;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch.LaunchActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Singleton;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.welcome.RedemptionModel;

public class RedeemActivity extends AppCompatActivity {

    EditText editText;
    String rCode;
    Button submitBtn;
    RedemptionModel redemptionModel;
    LinearLayout enterCodeLay;
    TextView responseMsg,thankyouVisit;
    RelativeLayout loaderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewar_claim);
        //rCode = getIntent().getStringExtra("redemptionCode");
        editText = (EditText) findViewById(R.id.code);
        enterCodeLay = (LinearLayout) findViewById(R.id.enterCodeLay);
        submitBtn = (Button) findViewById(R.id.submitBtn);
        thankyouVisit= (TextView) findViewById(R.id.thankyouVisit);
        loaderLayout = (RelativeLayout) findViewById(R.id.loaderLayout);
        responseMsg = (TextView) findViewById(R.id.responseMsg);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length() > 2) {
                    loaderLayout.setVisibility(View.VISIBLE);
                    callService(editText.getText().toString());
                } else {
                    editText.setError("Please enter valid code.");
                }
            }
        });
    }

    private void callService(String s) {
        new RedeemHandler().verifyCodeService(RedeemActivity.this, s, new RedeemInterface() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString) {
                loaderLayout.setVisibility(View.GONE);
                if (baseModel != null) {
                    redemptionModel = (RedemptionModel) baseModel;
                    enterCodeLay.setVisibility(View.GONE);
                    responseMsg.setVisibility(View.VISIBLE);
                    responseMsg.setText(redemptionModel.getMessage());
                    thankyouVisit.setVisibility(View.VISIBLE);
                    Singleton.getInstance().setFieldNull();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent=new Intent(RedeemActivity.this,LaunchActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }, 5000);

                } else {
                    Singleton.getInstance().showAlert(RedeemActivity.this,"Error occured make sure you have entered valid pin");
                    //Toast.makeText(RedeemActivity.this, "Some error occured,please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent=new Intent(RedeemActivity.this,LaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
