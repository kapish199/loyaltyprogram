package loyaltyprogram.codeplaylaps.com.loyaltyprogram.operatorAuthentication;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 24-Jul-17.
 */

public class AuthenticationModel extends BaseModel {
    private String amount="";
    private String customer_phonee="";
    private String message="";
    private String points="";
    private String points_added="";
    private String purchase_amount="";
    private String result="";
    private String success="";
    private String timestamp="";
    private String txn_id="";

    public String getAmount() {
        return amount;
    }

    public String getCustomer_phonee() {
        return customer_phonee;
    }

    public String getMessage() {
        return message;
    }

    public String getPoints() {
        return points;
    }

    public String getPoints_added() {
        return points_added;
    }

    public String getPurchase_amount() {
        return purchase_amount;
    }

    public String getResult() {
        return result;
    }

    public String getSuccess() {
        return success;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getTxn_id() {
        return txn_id;
    }
}
