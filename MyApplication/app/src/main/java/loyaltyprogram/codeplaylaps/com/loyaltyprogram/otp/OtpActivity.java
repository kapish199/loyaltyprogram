package loyaltyprogram.codeplaylaps.com.loyaltyprogram.otp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.R;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch.LaunchActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.RewardActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.RewardHandler;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.RewardResponse;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model.RewardWrapper;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Singleton;

public class OtpActivity extends AppCompatActivity {

    EditText enteredOtp;
    Button submitBtn;
    RelativeLayout loaderLayout;
    private OtpModel otpModel;
    private RewardWrapper rewardWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        loaderLayout= (RelativeLayout) findViewById(R.id.loaderLayout);
        new OtpHandler().getOtp(OtpActivity.this, Singleton.getInstance().getCustomerNum(), new OtpResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString) {

                loaderLayout.setVisibility(View.GONE);
                if(baseModel!=null){
                    //OTP received
                    /*Intent i=new Intent(OtpActivity.this, RewardActivity.class);
                    //i.putExtra("otp","547639");
                    startActivity(i);*/
                }else{
                    Toast.makeText(OtpActivity.this,"Please make sure you have an active internet", Toast.LENGTH_SHORT).show();
                }
            }
        });

        enteredOtp= (EditText) findViewById(R.id.otp);
        submitBtn= (Button) findViewById(R.id.submitBtn);
        onSubmitPressed();
    }
    private void onSubmitPressed(){
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp=enteredOtp.getText().toString();
                if(otp.length()>=4 && otp.length()<=6){
                    //call Verify Otp service
                    //pass opt via intent to reward activity
                    loaderLayout.setVisibility(View.VISIBLE);
                    verifyOtpAndFetchRewards(otp);
                    /*Intent intent=new Intent(OtpActivity.this,RewardActivity.class);
                    intent.putExtra("otp",otp);
                    startActivity(intent);*/
                }else{
                    enteredOtp.setError("Please enter valid otp.");
                }
            }
        });
    }

    private void verifyOtpAndFetchRewards(final String otp){

        new RewardHandler().rewardService(OtpActivity.this,Singleton.getInstance().getCustomerNum(),otp,new RewardResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString) {
                if(baseModel!=null){
                    rewardWrapper= (RewardWrapper) baseModel;
                    Singleton.getInstance().setRewardWrapper(rewardWrapper);
                    //makeView(rewardWrapper);

                    new OtpHandler().verifyOtp(OtpActivity.this, Singleton.getInstance().getCustomerNum(), otp, new OtpResponse() {
                        @Override
                        public void onResponse(BaseModel baseModel, String jsonString) {
                            loaderLayout.setVisibility(View.GONE);
                            if(baseModel!=null) {
                                otpModel = (OtpModel) baseModel;
                                //String encodedToken=otpModel.getToken();
                                if(otpModel.getMessage().equalsIgnoreCase("Authenticated successfully.")) {
                                    Intent intent = new Intent(OtpActivity.this, RewardActivity.class);
                                    intent.putExtra("otp", otp);
                                    intent.putExtra("token", otpModel.getToken());
                                    startActivity(intent);
                                }/*else{
                                    Singleton.showAlert(OtpActivity.this,"Invalid pin");
                                }*/
                            }else if(Integer.parseInt(jsonString)==401){
                                //Toast.makeText(OtpActivity.this,"Make sure you have active internet connection", Toast.LENGTH_SHORT).show();
                                Singleton.getInstance().showAlert(OtpActivity.this,"Invalid pin");
                            }
                        }
                    });

                }else{
                    //Reward service not recieved and show dialog or alert
                     loaderLayout.setVisibility(View.GONE);
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(OtpActivity.this, LaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
