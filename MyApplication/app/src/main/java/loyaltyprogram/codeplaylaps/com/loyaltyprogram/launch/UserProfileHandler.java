package loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch;

import android.app.Activity;
import android.content.Context;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseHandler;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.signup.SignUpParser;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Urls;

/**
 * Created by HP on 21-Jul-17.
 */

public class UserProfileHandler extends BaseHandler {

    public void getUserProfile(Activity context, String num, UserProfileResponse userProfileResponse){
        this.baseResponse=userProfileResponse;
        jsonGetRequest(context, Urls.USER_PROFILE +num,new UserProfileParser());
    }

    public void signUpService(Context activity, String name, String email, String num, UserProfileResponse userProfileResponse){
        this.baseResponse=userProfileResponse;
        //JSONObject params=new JSONObject();
        try {
            /*JSONObject user_data=new JSONObject();
            user_data.put("first_name",name);
            user_data.put("email",email);
            params.put("user_data",user_data);*/
            JsonStringRequest(activity,Urls.UPDATE_USER+"customer_name="+name+"&customer_phone="+num+"&email="+email,new SignUpParser());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
