package loyaltyprogram.codeplaylaps.com.loyaltyprogram.operatorAuthentication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.zxing.Result;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch.LaunchActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.otp.OtpActivity;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.R;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class AuthenticationActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {


    private ZXingScannerView mScannerView;
    private String num, amt;
    private RelativeLayout loaderLayout;
    private AuthenticationModel authenticationModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_authentication);
        loaderLayout = (RelativeLayout) findViewById(R.id.loaderLayout);
        num = getIntent().getStringExtra("phoneNumber");
        amt = getIntent().getStringExtra("amount");

    }

    public void openScanner(View view) {
        mScannerView = new ZXingScannerView(AuthenticationActivity.this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mScannerView != null) {
            mScannerView.stopCamera();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mScannerView != null) {
            mScannerView.startCamera();
        }
    }

    @Override
    public void handleResult(Result result) {
        //Do anything with result here :D
        Log.w("handleResult", result.getText());
        /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Scan result");
        builder.setMessage(result.getText());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();*/


        String headerValue = result.getText();

        mScannerView.stopCamera();
        mScannerView.stopCameraPreview();
        //setContentView(R.layout.activity_operator_authentication);
        //mScannerView.setVisibility(View.GONE);

        //loaderLayout.setVisibility(View.VISIBLE);
        //call service
        new AuthenticationHandler().sendAuthenticationService(AuthenticationActivity.this, num, amt, headerValue, new ServiceResponseInterface() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString) {
                /*mScannerView.stopCamera();
                mScannerView.stopCameraPreview();*/
                //loaderLayout.setVisibility(View.GONE);
                if (baseModel != null) {
                    authenticationModel= (AuthenticationModel) baseModel;
                   /* Intent intent=new Intent(AuthenticationActivity.this, RewardActivity.class);
                    intent.putExtra("authenticationModel",authenticationModel);*/

                    /*flag FLAG_ACTIVITY_NO_HISTORY is setting the noHistory true for
                    * RewardActivity*/
                    //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    Intent intent=new Intent(AuthenticationActivity.this, OtpActivity.class);

                    SharedPreferences pref=getSharedPreferences("myPref",MODE_PRIVATE);
                    SharedPreferences.Editor editor=pref.edit();
                    editor.putString("earnedPoints",authenticationModel.getPoints());
                    editor.putString("pointsAdded",authenticationModel.getPoints_added());
                    editor.commit();
                    /*intent.putExtra("authenticationModel",authenticationModel);*/
                    startActivity(intent);
                } else {
                    setContentView(R.layout.activity_operator_authentication);
                }
            }
        });

       /* mScannerView.stopCamera();
        mScannerView.stopCameraPreview();*/

        //setContentView(R.layout.activity_operator_authentication);

        /*//Resume scanning
        mScannerView.resumeCameraPreview(this);*/
    }

    @Override
    public void onBackPressed() {
        if (mScannerView != null) {
            mScannerView.stopCamera();
            mScannerView.stopCameraPreview();
            mScannerView = null;
            setContentView(R.layout.activity_operator_authentication);
        } else {
            //super.onBackPressed();
            Intent intent=new Intent(AuthenticationActivity.this, LaunchActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}
