package loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model;

import java.util.ArrayList;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 02-Aug-17.
 */

public class RewardWrapper extends BaseModel {
    ArrayList<RewardModel> locked;
    ArrayList<RewardModel> unlocked;

    public ArrayList<RewardModel> getLocked() {
        if(locked==null){
            locked=new ArrayList<>();
        }
        return locked;
    }

    public ArrayList<RewardModel> getUnlocked() {
        if(unlocked==null){
            unlocked=new ArrayList<>();
        }
        return unlocked;
    }
}
