package loyaltyprogram.codeplaylaps.com.loyaltyprogram.signup;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseParser;

/**
 * Created by HP on 24-Jul-17.
 */

public class SignUpParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseSignUpJson(response);
    }
    private BaseModel parseSignUpJson(JSONObject jsonObject){
        SignUpModel model;
        Gson gson=getGsonInstance();
        model=gson.fromJson(jsonObject.toString(),SignUpModel.class);
        return model;
    }
}
