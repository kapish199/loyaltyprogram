package loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model.RewardWrapper;

/**
 * Created by HP on 25-Jul-17.
 */

public class Singleton {
    private static final Singleton ourInstance = new Singleton();

    public static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton() {
    }
    private String customerName;
    private RewardWrapper rewardWrapper;



    private String customerNum;

    public RewardWrapper getRewardWrapper() {
        return rewardWrapper;
    }

    public void setRewardWrapper(RewardWrapper rewardWrapper) {
        this.rewardWrapper = rewardWrapper;
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public void setCustomerNum(String customerNum) {
        this.customerNum = customerNum;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setFieldNull() {
        customerNum=null;
        customerNum=null;
        rewardWrapper=null;
    }

    public  void showAlert(Context context,String msg){
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.setTitle(msg);
        //dialog.setMessage(msg);
        dialog.setCancelable(false);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

      /*  dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/
        dialog.show();
    }

}
