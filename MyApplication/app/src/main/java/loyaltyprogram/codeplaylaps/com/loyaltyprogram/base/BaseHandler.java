package loyaltyprogram.codeplaylaps.com.loyaltyprogram.base;

import android.app.Activity;
import android.content.Context;
import android.util.Log;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.utils.Constants;


/**
 * Created by mohit on 12/23/2016.
 */


abstract public class BaseHandler {

    protected BaseInterface baseResponse;

    protected void responseHandler(String jsonString, BaseParser parser) {
        if (jsonString.contains("qwaszx") || jsonString.contains("FAILURE") || jsonString.contains("qwerty")) {
            if (jsonString.contains("qwerty")) {
                String statusCode = jsonString.substring(0, 3);
                baseResponse.onResponse(null, statusCode);
            } else {
                baseResponse.onResponse(null, jsonString);
            }
            return;
        } else {
            try {
                baseResponse.onResponse(parser.parseJson(new JSONObject(jsonString)), jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * this is volley handler to call servers
     *
     * @param activity
     * @param url
     * @param params
     * @param RESPONSE_CLASS
     */
    protected void jsonRequest(final Activity activity, String url, HashMap<String, Object> params, final int RESPONSE_CLASS) {
        Log.e("Class params", params.toString());
        Log.e("Class params", url);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Response", response.toString());
                        //ResponseClassCollection.getResponse(RESPONSE_CLASS,response,activity);//parseLoginJsonResponse(response);
                        //progressdialog.dismiss();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Log.e("Volly Error", error.toString());
                //responseJsonBaseInterface.onServiceResponseHandler(null,false,error.toString());
                NetworkResponse networkResponse = error.networkResponse;

                if (networkResponse != null) {
                    Log.e("Status code", String.valueOf(networkResponse.statusCode));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        req.setShouldCache(false);
        //   RequestQueue requestQueue = Volley.newRequestQueue((Context)responseJsonBaseInterface);
        //  requestQueue.add(req);
    }


    /**
     * This method is called create the json and use volley service
     *
     * @param activity
     * @param url
     * @param params
     */
    /*protected void jsonPostHeaderRequest(final Context activity, String url, String params, final String headerValue, final BaseParser baseParser) {
        Log.e("Class params", params.toString());
        Log.e("Class params", url);
        JsonObjectRequest req = null;
        try {
            req = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Response", response.toString());
                            //              ResponseClassCollection.getResponse(RESPONSE_CLASS,response,responseJsonBaseInterface);//parseLoginJsonResponse(response);
                            //        progressdialog.dismiss();
                            //baseResponse.onResponse(null,response.toString());
                            BaseHandler.this.responseHandler(response.toString(), baseParser);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error.getMessage());
                    Log.e("Volly Error", error.toString());
                    //baseResponse.onResponse(null,"Error");
                    BaseHandler.this.responseHandler("qwaszx", baseParser);
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null) {
                        Log.e("Status code", String.valueOf(networkResponse.statusCode));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json"*//*; charset=utf-8"*//*);
                    headers.put("Authorization",headerValue);
                    Log.e("Authorization",headers.get("Authorization"));
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    if (response.statusCode != 200) {
                        //      baseResponse.onResponse(null,null);
                    }
                    return super.parseNetworkResponse(response);
                }
            };
        } catch (JSONException e) {
            //progressdialog.dismiss();
            //loginResponse.onResponse(null,"Json Error");
            BaseHandler.this.responseHandler("Json Error", baseParser);
            e.printStackTrace();
        }

        req.setShouldCache(false);
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(req);
    }*/
    public void stringHeaderRequest(final Context activity, String url, final String headerValue,final BaseParser baseParser){

        Log.e("Class params", url);
        StringRequest req = null;
        req = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response.toString());
                        //              ResponseClassCollection.getResponse(RESPONSE_CLASS,response,responseJsonBaseInterface);//parseLoginJsonResponse(response);
                        //        progressdialog.dismiss();
                        //baseResponse.onResponse(null,response.toString());
                        BaseHandler.this.responseHandler(response.toString(), baseParser);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == 301 || error.networkResponse.statusCode == 302) {
                    String loc = error.networkResponse.headers.get("Location");
                    Log.e("location", loc);
                    jsonGetRequest(activity, loc, baseParser);
                } else {
                    VolleyLog.e("Error: ", error.getMessage());
                    Log.e("Volly Error", error.toString());
                    //baseResponse.onResponse(null,"Error");
                    BaseHandler.this.responseHandler(error.networkResponse.statusCode + " qwerty", baseParser);
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null) {
                        Log.e("Status code", String.valueOf(networkResponse.statusCode));
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                        headers.put("Authorization",headerValue);
                Log.e("Authorization",headers.get("Authorization"));
                return headers;
            }
        };
        req.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(req);

    }


    public void JsonStringRequest(final Context activity, String url, final BaseParser baseParser){

        Log.e("Class params", url);
        StringRequest req = null;
        req = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response.toString());
                        //              ResponseClassCollection.getResponse(RESPONSE_CLASS,response,responseJsonBaseInterface);//parseLoginJsonResponse(response);
                        //        progressdialog.dismiss();
                        //baseResponse.onResponse(null,response.toString());
                        BaseHandler.this.responseHandler(response.toString(), baseParser);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == 301 || error.networkResponse.statusCode == 302) {
                    String loc = error.networkResponse.headers.get("Location");
                    Log.e("location", loc);
                    jsonGetRequest(activity, loc, baseParser);
                } else {
                    VolleyLog.e("Error: ", error.getMessage());
                    Log.e("Volly Error", error.toString());
                    //baseResponse.onResponse(null,"Error");
                    BaseHandler.this.responseHandler(error.networkResponse.statusCode + " qwerty", baseParser);
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null) {
                        Log.e("Status code", String.valueOf(networkResponse.statusCode));
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("authorization", "apikey "+Constants.USER_NAME +":"+Constants.API_KEY);
                return headers;
            }
        };
        req.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(req);

    }

    public void jsonGetRequest(final Context activity, String url, final BaseParser baseParser) {
        Log.e("Class params", url);
        StringRequest req = null;
        req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response.toString());
                        //              ResponseClassCollection.getResponse(RESPONSE_CLASS,response,responseJsonBaseInterface);//parseLoginJsonResponse(response);
                        //        progressdialog.dismiss();
                        //baseResponse.onResponse(null,response.toString());
                        BaseHandler.this.responseHandler(response.toString(), baseParser);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode == 301 || error.networkResponse.statusCode == 302) {
                    String loc = error.networkResponse.headers.get("Location");
                    Log.e("location", loc);
                    jsonGetRequest(activity, loc, baseParser);
                } else {
                    VolleyLog.e("Error: ", error.getMessage());
                    Log.e("Volly Error", error.toString());
                    //baseResponse.onResponse(null,"Error");
                    BaseHandler.this.responseHandler(error.networkResponse.statusCode + " qwerty", baseParser);
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null) {
                        Log.e("Status code", String.valueOf(networkResponse.statusCode));
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("authorization", "apikey "+Constants.USER_NAME +":"+Constants.API_KEY);
                return headers;
            }
        };
        req.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(req);

    }

  /*  public void jsonDeleteRequest(final Context activity, String url, final BaseParser baseParser) {
        Log.e("Class params", url);
        StringRequest req = null;
        req = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("Response", response.toString());
                BaseHandler.this.responseHandler(response.toString(), baseParser);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Log.e("Volly Error", error.toString());
                //baseResponse.onResponse(null,"Error");
                BaseHandler.this.responseHandler("qwaszx", baseParser);
                NetworkResponse networkResponse = error.networkResponse;

                if (networkResponse != null) {
                    Log.e("Status code", String.valueOf(networkResponse.statusCode));
                }
            }
        });
        req.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(req);
    }
*/
    public  void jsonPutRequest(Activity context, String params, String url, final BaseParser baseParser){
        Log.e("Class params", params.toString());
        Log.e("Class params", url);
        JsonObjectRequest req = null;
        try {
            req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(params),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Response", response.toString());

                            //              ResponseClassCollection.getResponse(RESPONSE_CLASS,response,responseJsonBaseInterface);//parseLoginJsonResponse(response);
                            //        progressdialog.dismiss();
                            //baseResponse.onResponse(null,response.toString());
                            BaseHandler.this.responseHandler(response.toString(), baseParser);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error.getMessage());
                    Log.e("Volly Error", error.toString());
                    //baseResponse.onResponse(null,"Error");
                    BaseHandler.this.responseHandler(error.networkResponse.statusCode + " qwerty", baseParser);
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null) {
                        Log.e("Status code", String.valueOf(networkResponse.statusCode));
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "apikey "+Constants.USER_NAME +":"+Constants.API_KEY);
                    Log.e("Authorization",headers.get("Authorization"));
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    if (response.statusCode != 200) {
                        //      baseResponse.onResponse(null,null);
                    }
                    return super.parseNetworkResponse(response);
                }
            };
        } catch (JSONException e) {
            //progressdialog.dismiss();
            //loginResponse.onResponse(null,"Json Error");
            BaseHandler.this.responseHandler("Json Error", baseParser);
            e.printStackTrace();
        }

        req.setShouldCache(false);
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(req);

    }
}
