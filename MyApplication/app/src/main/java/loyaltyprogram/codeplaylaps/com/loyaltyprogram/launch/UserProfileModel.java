package loyaltyprogram.codeplaylaps.com.loyaltyprogram.launch;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;

/**
 * Created by HP on 21-Jul-17.
 */

public class UserProfileModel extends BaseModel {
    private String first_name="";
    private String last_name="";
    private String email="";
    private String phone="";
    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }



}
