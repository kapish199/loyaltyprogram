package loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseParser;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.reward.model.RedeemModel;

/**
 * Created by HP on 03-Aug-17.
 */

public class RedeemCodeParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseResponseJson(response);
    }

    private BaseModel parseResponseJson(JSONObject response) {
        RedeemModel redeemModel=new RedeemModel();
        try {
            redeemModel.setRedemption_code(response.getString("redemption_code"));
            return redeemModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
