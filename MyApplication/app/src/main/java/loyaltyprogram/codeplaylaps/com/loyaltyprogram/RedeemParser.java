package loyaltyprogram.codeplaylaps.com.loyaltyprogram;

import org.json.JSONException;
import org.json.JSONObject;

import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseModel;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.base.BaseParser;
import loyaltyprogram.codeplaylaps.com.loyaltyprogram.welcome.RedemptionModel;

/**
 * Created by HP on 04-Aug-17.
 */

class RedeemParser extends BaseParser {

    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseResponseJson(response);
    }
    public BaseModel parseResponseJson(JSONObject response) {
        RedemptionModel model=new RedemptionModel();
        try {
            model.setMessage(response.getString("message"));
            return model;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
}
